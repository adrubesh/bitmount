#ifndef UI_H
#define UI_H

#include <gtk/gtk.h>

struct ui_components {
	GtkWindow *window;	
	GtkEntry *source_entry;
	GtkEntry *dest_entry;
	GtkButton *source_button;
	GtkButton *dest_button;
	GtkRadioButton *recovery_radio;
	GtkRadioButton *password_radio;
	GtkEntry  *key_entry;
	GtkButton *mount_button;
	GtkButton *unmount_button;
};

struct ui_components *initialize_ui();

#endif
