#include "ui.h"

int main (int argc, char **argv)
{
	gtk_init(&argc, &argv);
	struct ui_components *ui = initialize_ui();
	gtk_main();
	
	free(ui);
	return 0;
}
