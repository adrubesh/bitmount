export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
FLAGS = $(shell pkg-config --cflags libbde gtk+-3.0)
CFLAGS = -Wall -g -Werror $(FLAGS) 
LIBS = $(shell pkg-config --libs libbde gtk+-3.0)
TARGET = bitmount

OBJS = ui.o main.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $(TARGET) $(LIBS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< $(LIBS)



