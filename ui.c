#include "ui.h"

static void open_dialog(GtkWidget *widget, gpointer data)
{
	struct ui_components *ui = data;
	GtkFileChooserNative *fc;
	GtkFileChooserAction action;
	GtkEntry *dest;
	gint res;

	if((GtkButton *)widget == ui->source_button) {
		action = GTK_FILE_CHOOSER_ACTION_OPEN;
		dest = ui->source_entry;
	} else if ((GtkButton *)widget == ui->dest_button) {
		action = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
		dest = ui->dest_entry;
	}

	fc = gtk_file_chooser_native_new("Open", ui->window, action, "_Open", "_Cancel");
	res = gtk_native_dialog_run(GTK_NATIVE_DIALOG(fc));
	if(res == GTK_RESPONSE_ACCEPT) {
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER(fc);
		filename = gtk_file_chooser_get_filename(chooser);

		gtk_entry_set_text(dest, filename);
	}

	g_object_unref(fc);
}

struct ui_components *initialize_ui()
{
	GtkBuilder *builder;
	GError *error = NULL;
	struct ui_components *ui = malloc(sizeof(struct ui_components));

	
	builder = gtk_builder_new();
	if(gtk_builder_add_from_file(builder, "UI.glade", &error) == 0) {
		g_printerr("Error loading file: %s\n", error->message);
		g_clear_error(&error);
		return NULL;
	}

	ui->window = GTK_WINDOW(gtk_builder_get_object(builder, "window"));
	g_signal_connect(ui->window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	ui->source_button = GTK_BUTTON(gtk_builder_get_object(builder, "source_button"));
	g_signal_connect(ui->source_button, "clicked", G_CALLBACK(open_dialog), ui);

	ui->dest_button = GTK_BUTTON(gtk_builder_get_object(builder, "dest_button"));
	g_signal_connect(ui->dest_button, "clicked", G_CALLBACK(open_dialog), ui);

	ui->source_entry = GTK_ENTRY(gtk_builder_get_object(builder, "source_entry"));
	ui->dest_entry = GTK_ENTRY(gtk_builder_get_object(builder, "dest_entry"));

	return ui;	
}
